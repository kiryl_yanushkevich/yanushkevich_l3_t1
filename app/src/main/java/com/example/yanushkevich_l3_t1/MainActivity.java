package com.example.yanushkevich_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String APP_PREFERENCES = "signinsettings";
    public static final String APP_PREFERENCES_LOGIN = "Email";
    public static final String APP_PREFERENCES_PASSWORD = "Password";

    SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

    }

    public void onSignInButtonClick(View view){
        if (mSettings.contains(APP_PREFERENCES_LOGIN) && mSettings.contains(APP_PREFERENCES_PASSWORD)) {
            Intent intent = new Intent(this, HistoryListWindow.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, SignInWindow.class);
            startActivity(intent);
        }
    }

    public void onSignUpButtonClick(View view){
        Intent intent = new Intent(this, SignUpWindow.class);
        startActivity(intent);
    }
}
