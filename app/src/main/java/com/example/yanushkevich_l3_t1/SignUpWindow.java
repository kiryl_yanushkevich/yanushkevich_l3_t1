package com.example.yanushkevich_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SignUpWindow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_window);
    }

    public void onSignUpButtonClick(View view){
        Intent intent = new Intent(this, HistoryListWindow.class);
        startActivity(intent);
    }
}
