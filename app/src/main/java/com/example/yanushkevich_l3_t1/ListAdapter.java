package com.example.yanushkevich_l3_t1;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.HistoryViewHolder> {
    public static class HistoryViewHolder extends RecyclerView.ViewHolder{
        CardView historyCardView;
        TextView paragrapf;
        TextView cardText;

        HistoryViewHolder(final View itemView) {
            super(itemView);
            historyCardView = (CardView)itemView.findViewById(R.id.history_card_view);
            paragrapf = (TextView)itemView.findViewById(R.id.card_title);
            cardText = (TextView)itemView.findViewById(R.id.card_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemView.getContext().startActivity(new Intent(itemView.getContext(), DetailHistory.class));
                }
            });
        }
    }

    @Override
    public int getItemCount(){
        return 20;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.person_history_card, viewGroup, false);
        HistoryViewHolder hvh = new HistoryViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder historyViewHolder, int i){
        historyViewHolder.cardText.setText(R.string.card_text);
        historyViewHolder.paragrapf.setText(R.string.card_title);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
