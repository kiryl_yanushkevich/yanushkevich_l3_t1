package com.example.yanushkevich_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

public class SignInWindow extends AppCompatActivity {

    public static final String APP_PREFERENCES = "signinsettings";
    public static final String APP_PREFERENCES_LOGIN = "Email";
    public static final String APP_PREFERENCES_PASSWORD = "Password";

    SharedPreferences mSettings;
    EditText email, password;
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_window);

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        email = findViewById(R.id.sign_in_email);
        password = findViewById(R.id.sign_in_password);
        checkBox = findViewById(R.id.sign_in_password_checkbox);
    }

    public void onSignInButtonClick(View view){
        if (check()){
            setPreferences();
        }

        Intent intent = new Intent(this, HistoryListWindow.class);
        startActivity(intent);

    }

    private boolean isEdittextContains(){
        if (password.getText().toString().equals("") || email.getText().toString().equals("")){
            return false;
        } else {
            return true;
        }
    }

    private boolean check(){
        if (checkBox.isChecked() && isEdittextContains()){
            return true;
        } else {
            return false;
        }
    }

    private void setPreferences(){
        String emailText = email.getText().toString();
        String passwordText = password.getText().toString();
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(APP_PREFERENCES_LOGIN, emailText);
        editor.putString(APP_PREFERENCES_PASSWORD, passwordText);
        editor.apply();
    }
}
