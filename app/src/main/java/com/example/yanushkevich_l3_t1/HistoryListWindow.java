package com.example.yanushkevich_l3_t1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class HistoryListWindow extends AppCompatActivity {

    public static final String APP_PREFERENCES = "signinsettings";
    public static final String APP_PREFERENCES_LOGIN = "Email";
    public static final String APP_PREFERENCES_PASSWORD = "Password";

    SharedPreferences mSettings;
    Button logOutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list_window);
        logOutButton = findViewById(R.id.log_out_button);
        mSettings = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
        showHideButton();

        RecyclerView rv = findViewById(R.id.history_list);

        ListAdapter listAdapter = new ListAdapter();
        rv.setAdapter(listAdapter);
    }

    public void onLogOutButtonClick(View view){
         SharedPreferences.Editor editor = mSettings.edit();
         editor.remove(APP_PREFERENCES_LOGIN);
         editor.remove(APP_PREFERENCES_PASSWORD);
         editor.apply();
         logOutButton.setVisibility(View.GONE);
    }

    private void showHideButton(){
        if (mSettings.contains(APP_PREFERENCES_LOGIN) && mSettings.contains(APP_PREFERENCES_PASSWORD)) {
            logOutButton.setVisibility(View.VISIBLE);
        } else {
            logOutButton.setVisibility(View.GONE);
        }
    }
}
